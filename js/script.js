(function($){
	/// slider detail
    $('[data-slider="true"]').each(function(e){
    	var $visible 	= $(this).attr('data-slider-visible') || 1;
    	var $duration 	= $(this).attr('data-slider-duration')|| 1000;

    	$(this).wrap('<div class="b-slider-viewport"></div>');

    	if($(this).children('li').length > $visible)
    	{
    		$(this).parent().after('<ul class="b-slider-control"><li class="b-slider-control-item prev"><a href="javascript:;">Previus</a></li> <li class="b-slider-control-item next"><a href="javascript:;">Next</a></li></ul>');
			var animate = false;
			var $el;

			$(this).parent().next('.b-slider-control').find('.b-slider-control-item > a').click(function(e){
				if(animate == true) return;
				
				animate = true;
				setTimeout(function(){animate = false;},$duration);
				
				var $slider = $(this).parents('.b-slider-control').prev('.b-slider-viewport').children('[data-slider="true"]');
				
				if($(this).parent().hasClass('prev'))
				{
					$el = $slider.children('li:last-child');
					$w  = $el.outerWidth(true);
					$slider.css({left:-$w}).animate({left:0},$duration).prepend($el.detach());
					$(document).click();
				} 
				else if($(this).parent().hasClass('next')) 
				{
					$el = $slider.children('li:first-child');
					$w  = $el.next().outerWidth(true);
					$slider.animate({left:-$w},$duration,function(){ $slider.append($el.detach()).css({left:0}); });
				}
			});
    	}
    });

	
	
	$('[data-validator]').live('blur',function(){
		checkInput(this);
	});
	$('form').live('submit',function(e){
		var state = true, $form = $(this);
	    e.preventDefault();
		$('[data-validator]',this).each(function(i,el){
	    	if(!checkInput(el))
	    	{
				state = false;
	    	}
	    });
	    if( !state ) return false;
		  	
	  	$.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: 'POST',
            success: function(data)
            {
                if(data['response'] =='success')
                {
                    generateSuccess(this);
                }
                else
                {
                    $("<div class='b-wrapp b-alert error'><div class='b-alert-message'><span>"+data['msg']+"</span></div><a href='javascript:$.modal('close');' class='b-modal-close'></a></div>").modal();
                }
            }
    	});
	});
	
	function generateSuccess()
	{
	    var str = 'Спасибо за вашу заявку!';
	    var popupTemplate = "<div class='b-wrapp b-alert success'><div class='b-alert-message'><span>{message}</span></div><a href='javascript:$.modal('close');' class='b-modal-close'></a></div>";
	    $(popupTemplate.replace('{message}',str)).modal();
	}

	function checkInput($element)
	{
		if(validator($element))
		{
			$($element).removeClass('error').removeAttr('style');
			return true;
		}
		else
		{
			$($element).addClass('error').css({borderColor:'#FF0000'});
			return false;
		}
	};
	function validator($element)
	{
		var $validator 		= $($element).attr('data-validator'),
			$validatorName 	= $validator.split('=')[0],
			$arg 			= 0;
		if( $($element).is('input,select,textarea') && $validator.length > 0 && typeof($validatorName) !='undefined' )
		{
			var $value 		= $($element).val(),
				$args 		= $validator.split('=');

			if( typeof($args[1]) == 'string' && $args[1].length > 0 )
				$arg = $args[1];

			if($validatorName == 'min' )
			{ return (($arg > 0 && $value.length > $arg ) || $value.length > 1)?true:false; }
			
			if($validatorName == 'number' )
			{ return /^[\d]+$/gi.test($value); }

			if($validatorName == 'email' )
			{ return /.+@.+\..+/.test($value); }
			if($validatorName == 'date' )
			{ return /^(((0[1-9]|[12][0-9]|3[01])([\.])(0[13578]|10|12)([\.])(\d{4}))|(([0][1-9]|[12][0-9]|30)([\.])(0[469]|11)([\.])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([\.])(02)([\.])(\d{4}))|((29)(\.|-|\/)(02)([\.])([02468][048]00))|((29)([\.])(02)([\.])([13579][26]00))|((29)([\.])(02)([\.])([0-9][0-9][0][48]))|((29)([\.])(02)([\.])([0-9][0-9][2468][048]))|((29)([\.])(02)([\.])([0-9][0-9][13579][26])))$/.test($value);
			}
			if($validatorName == 'telephone' )
			{ return /^[\+]{0,1}[\s\d]+$/.test($value); }
		}
		return false;
	}


	$(".js-modal__call").click(function(e){
		e.preventDefault();
	    $.modal({content: $($(this).attr('data-call')).clone() });
	});

})(jQuery);